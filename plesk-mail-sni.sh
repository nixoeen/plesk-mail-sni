#!/bin/bash

# Enabling SSL for mail subdomains in Plesk
# Copyright (C) 2024 Moein Alinaghian <nixoeen at nixoeen.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# List of certificates and their creation and modification dates
SNAPSHOT="/opt/plesk-mail-sni/plesk-mail-sni.dir"
TEMP_SNAPSHOT=$(mktemp)

# Path of configuration files and certificates
PATH_LETSENCRYPT="/opt/psa/var/modules/letsencrypt/etc/live"
PATH_DOVECOT="/etc/dovecot/conf.d/16-anormeda-sni.conf"
PATH_POSTFIX="/etc/postfix/anormeda-sni"

# Check if there is any changes in certificates
ls -l "$PATH_LETSENCRYPT/mail."* > "$TEMP_SNAPSHOT"
if ! diff "$SNAPSHOT" "$TEMP_SNAPSHOT" > /dev/null; then
	TEMP_DOVECOT=$(mktemp)
	TEMP_POSTFIX=$(mktemp)

	# Generating the configuration files in a temporary place
	find "$PATH_LETSENCRYPT" -mindepth 1 -maxdepth 1 -type d -name "mail.*" -print0 | while IFS= read -r -d '' DIR; do
		HOST=$(basename "$DIR")
		echo "local_name $HOST {" >> $TEMP_DOVECOT
		echo "  ssl_cert = <$PATH_LETSENCRYPT/$HOST/fullchain.pem" >> $TEMP_DOVECOT
		echo "  ssl_key = <$PATH_LETSENCRYPT/$HOST/privkey.pem" >> $TEMP_DOVECOT
		echo "}" >> $TEMP_DOVECOT
		echo "$HOST $PATH_LETSENCRYPT/$HOST/privkey.pem $PATH_LETSENCRYPT/$HOST/fullchain.pem" >> $TEMP_POSTFIX
	done

	# Overwrite the old configuration files
	cp "$TEMP_DOVECOT" "$PATH_DOVECOT"
	cp "$TEMP_POSTFIX" "$PATH_POSTFIX"
	cp "$TEMP_SNAPSHOT" "$SNAPSHOT"
	chmod 644 "$PATH_DOVECOT"
	chmod 644 "$PATH_POSTFIX"

	# Update the Postfix lookup table
	/usr/sbin/postmap -F "$PATH_POSTFIX"

	# Restart the daemons
	/usr/bin/systemctl reload dovecot
	/usr/bin/systemctl reload postfix

	# Delete temporary files
	rm -f "$TEMP_DOVECOT"
	rm -f "$TEMP_POSTFIX"
fi

# Delete temporary files
rm -f "$TEMP_SNAPSHOT"

